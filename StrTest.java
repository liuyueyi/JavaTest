import java.util.Date;
import java.text.*;
public class StrTest{
    public void dateTest(){
        long time = System.currentTimeMillis();
        System.out.println("the time is " + time);
        Date nowTime = new Date(1385631521l);
        SimpleDateFormat sdFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String retStrFormat = sdFormatter.format(nowTime);
        System.out.println(retStrFormat);

    }

    public void strReplaceTest(){
        String value = "hello %world%";
	String value2 = value.replace("world", "nihao");
        System.out.println(value + "  " + value2);
	value = "welcome ${nihao}";
	value2 = value.replaceAll("\\$\\{nihao\\}", "中国");
	System.out.println(value2);
    }

    public static void main(String[] args){
        StrTest hello = new StrTest();
        hello.dateTest();   
        hello.strReplaceTest();
    }
}
